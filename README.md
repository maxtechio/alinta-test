### Alinta Energy Coding Test
As part of your coding test, Alinta Energy needs to identify your strengths as a developer. We've developed an API for you to consume in your application, which is provided at this swagger endpoint.
 
Please build an application that connects to the API, and produces a list of characters played in films, grouped by the actors name, and sorted by the film's name.
Please choose the language and frameworks that you are most passionate about - we'll discuss the result of the test and the code that you've written with you in a further stage of your interviews with us. We're happy to review any code in any languages. 

Please also use frameworks or code that simplify your solution, such as LINQ or Underscore.js.

## Notes
- Due to the Alinta API not supporting CORS for any domain, retrival of movie data is performed at build time

## Available Scripts

In the project directory, you can run:

### `npm install`

Will install all the dependencies required to run the application

### `npm test`

Runs the 'jest' test files to validate application logic is correct

### `npm run build`

Runs the './scripts/build' file that connects to the Alinta Test Webservice and pulls down defined movies.
Manipulates the data and saves the output in the './src/data' directory

It then compiles the 'TS' files

And Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

