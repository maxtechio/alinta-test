
jest.mock('node-fetch');
const fetch = require("node-fetch")
const MovieAPI = require("../../../scripts/api/movie")

describe("Integration Test: Connect to the Alinta API", () => {

  test("Make a request to api/Movies", (done) => {

    fetch.mockImplementationOnce( require.requireActual('node-fetch').default )

    MovieAPI.get()
      .then(json => {

        expect(Array.isArray(json)).toBeTruthy();
        json.forEach(item => {
          expect(typeof item === 'object').toBeTruthy()
        })
        done();

      })
      .catch(error => fail(error))

  })

  test("Gracefully handle exception in API request", (done) => {

    fetch.mockImplementationOnce(() => Promise.reject("Forced Exception!"))

    MovieAPI.get()
      .then(json => { fail(); done(); })
      .catch(error => {
        expect(error).toBe("Forced Exception!");
        done();
      })

  })



})