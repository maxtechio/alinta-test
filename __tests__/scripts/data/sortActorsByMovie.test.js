
const DataManipulate = require("../../../scripts/data/manipulate") 

const actorPayload = {
  "Seth Green": [
    {
      "movie": "Movie B",
      "character": "Chris Griffin"
    },
    {
      "movie": "Movie C",
      "character": "Luke Skywalker"
    },
    {
      "movie": "Movie A",
      "character": "Luke Skywalker"
    }
  ],
}

describe("A collection of tests for function sortActorsByMovie", () => {

  test("Can sort actors payload by movie", (done) => {

    const sortedActors = DataManipulate.sortActorsByMovie(actorPayload);
    const seth = sortedActors["Seth Green"];

    expect(seth[0].movie).toBe("Movie A");
    expect(seth[1].movie).toBe("Movie B");
    expect(seth[2].movie).toBe("Movie C");
    done();

  })

})