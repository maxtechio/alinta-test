

const DataManipulate = require("../../../scripts/data/manipulate") 

const moviePayload = [
  {
    "name": "Family Guy",
    "roles": [
      {
        "name": "Meg Griffin",
        "actor": "Mila Kunis"
      },
      {
        "name": "Meg Griffin",
        "actor": "Mila Kunis"
      },
      {
        "name": "Chris Griffin",
        "actor": "Seth Green"
      },
      {
        "name": "Luke Skywalker",
        "actor": "Seth Green"
      },
      {
        "name": "Joe Swanson"
      },
      {
        "name": "Lois Griffin",
        "actor": "Alex Borstein"
      }
    ]
  }
]

describe("A collection of tests for function groupByActors", () => {

  test("Returns an object of actors and there roles", (done) => {

    const actors = DataManipulate.groupByActors(moviePayload);
    const actorNames = Object.keys(actors);

    expect(actorNames.length).toBe(3);
    expect(actorNames.includes("Seth Green")).toBeTruthy();
    expect(actorNames.includes("Mila Kunis")).toBeTruthy();
    expect(actorNames.includes("Alex Borstein")).toBeTruthy();

    done();

  })

})