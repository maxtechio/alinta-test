
const DataManipulate = require("../../../scripts/data/manipulate") 

describe("A collection of tests for function roleAlreadyExsists", () => {

  test("Returns true when role already exists in the actor array", (done) => {

    const newRole = { movie: "Family Guy", character: "Meg Griffin" }
    const actorArray = [ newRole ];

    const result = DataManipulate.roleAlreadyExists(actorArray, newRole);

    expect(result).toBeTruthy()
    done()

  })

  test("Returns false when role doesn't exist in the actor array", (done) => {

    const newRole = { movie: "Family Guy", character: "Meg Griffin" }
    const actorArray = [  ];

    const result = DataManipulate.roleAlreadyExists(actorArray, newRole);

    expect(result).toBeFalsy()
    done()

  })

})