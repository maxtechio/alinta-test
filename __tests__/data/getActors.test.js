
const moviePayload = require("../../__mocks__/movie/get.json")

describe("Get Actors Tests", () => {

  test("PROOF: Find all the actors in the payload", (done) => {

    const actors = {};

    moviePayload.forEach(movie => {

      movie.roles.forEach(role => {
        if (!actors[role.actor])
          actors[role.actor] = [];
        
        actors[role.actor].push({
          movie: movie.name,
          character: role.name
        });
      })

    })

    console.log(actors)
    done();

  })

})