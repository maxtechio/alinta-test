
//react-scripts build
const colors = require("colors");

const MovieAPI = require("./api/movie");
const DataManipulate = require("./data/manipulate");
const DataWrite = require("./data/write");

MovieAPI.get()
  .then(json => DataManipulate.groupByActors(json))
  .then(json => DataManipulate.sortActorsByMovie(json))
  .then(json => DataWrite.writeActorFile(json))
  .then(json => {
    console.log("SUCCESS:".green, "All movie files generated!")
    console.log("");
  })
  .catch(error => {
    console.log("ERROR:".red, "Failed to generate movie files")
    console.log(error);
    console.log("");
    process.exit(1);
  })


