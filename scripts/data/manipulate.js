
exports.groupByActors = (moviePayload) => {

  const actors = {};

  moviePayload.forEach(movie => {

    movie.roles.forEach(role => {

      if (!role.actor)
        return;

      actorRoles = actors[role.actor] ? actors[role.actor] : [];

      const newRole = {
        movie: movie.name,
        character: role.name
      }

      if (!this.roleAlreadyExists(actorRoles, newRole))
        actorRoles.push(newRole);

      actors[role.actor] = actorRoles;

    })

  })

  return actors;

}

exports.roleAlreadyExists = (actorRoles, newRole) => {
  return actorRoles.find(role => {
    return (role.movie == newRole.movie
              && role.character == newRole.character)
  })
}

exports.sortActorsByMovie = (actorsPayload) => {

  const movieSort = (a, b) => {

    if (a.movie === undefined || b.movie === undefined)
      return 1;
      
    aMovie = a.movie.toUpperCase();
    bMovie = b.movie.toUpperCase();

    if (aMovie < bMovie) return -1;
    if (aMovie > bMovie) return 1;
    else return 0;
  }

  Object.keys(actorsPayload)
    .forEach(actor => {
      actorsPayload[actor].sort(movieSort);
    })
  
  return actorsPayload;

}