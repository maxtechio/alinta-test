
const fs = require("fs");
const path = require("path");
const colors = require("colors");

exports.writeActorFile = (actorJSON) => new Promise((resolve) => {

  const dataJSONPath = path.join(__dirname, "../../", "/src/data")

  if (!fs.existsSync(dataJSONPath))
    fs.mkdirSync(dataJSONPath)

  const actorsJSONPath = path.join(dataJSONPath, "actors.json");
  const data = JSON.stringify(actorJSON, null, 2)
  fs.writeFile(actorsJSONPath, data, (error) => {

    if (error)
      console.log("ERROR:".red, `Error writing [actor.json] file to [${dataJSONPath}]`);
    else
      console.log("SUCCESS:".green, `Created [actor.json] file to [${dataJSONPath}]`);

    resolve(actorsJSONPath);

  })
})