const fetch = require("node-fetch");

exports.get = () => {

  const method = "GET";
  const headers = {
      "Accept": "application/json",
      "Content-Type": "application/json"
  };
  const url = 'https://alintacodingtest.azurewebsites.net/api/Movies';
  
  return fetch(url, { method, headers })
      .then(response => response.json());

};
