import React from 'react';
import logo from '../assets/img/logo.svg';
import './App.css';
class App extends React.Component {
    render() {
        return (React.createElement("div", { className: "App" },
            React.createElement("header", { className: "App-header" },
                React.createElement("img", { src: logo, className: "App-logo", alt: "logo" }),
                React.createElement("h1", { className: "App-title" }, "Welcome to React")),
            React.createElement("p", { className: "App-intro" },
                "To get started, edit ",
                React.createElement("code", null, "src/App.js"),
                " and save to reload.")));
    }
}
export default App;
