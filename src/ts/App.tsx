import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { ListGroup, ListGroupItem, Badge } from 'reactstrap';
import { Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle } from 'reactstrap';

import 'bootstrap/dist/css/bootstrap.min.css';
import '../assets/css/App.css';

declare var require: any;
const actorsDataFile = require("../data/actors.json");

class App extends React.Component<any, any> {

  constructor(props){
    super(props);
    this.state = {
      actors: this.prepareActors()
    }
  }

  prepareActors(){
    const sortedActors = Object.keys(actorsDataFile).sort();
    return sortedActors.map((actor) => {
      return { name: actor, roles: actorsDataFile[actor] }
    })
  }

  render() {
    return (
      <div className="App">
        <Container fluid={false}>
          <Row>
            {
              this.state.actors.map((actor, index) => {

                return (
                    <Col xs="4" key={index}>
                      <Card>
                        {/* <CardImg top width="100%" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180" alt="Card image cap" /> */}
                        <CardBody>
                          <CardTitle>{actor.name}</CardTitle>
                          <CardSubtitle>Roles</CardSubtitle>
                          <ListGroup>
                            {
                              actor.roles.map((role, index) => {
                                return <ListGroupItem key={index}>
                                        {role.character}
                                        <br/><Badge>{role.movie}</Badge>
                                      </ListGroupItem>
                              })
                            }
                          </ListGroup>
                        </CardBody>
                      </Card>
                    </Col>
                )

              })
            }
          </Row>
        </Container>
      </div>
    );
  }
}

export default App;
